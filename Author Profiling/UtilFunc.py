import pickle
from nltk.corpus.reader.xmldocs import XMLCorpusReader
from nltk.tokenize import sent_tokenize, word_tokenize
import nltk
import numpy as np
import pickle
from textstat.textstat import textstat
from bs4 import BeautifulSoup
import HTMLParser
from pattern.en import parse
from nltk.tag import pos_tag, map_tag
from nltk.util import bigrams


def create_feature_vect(file_name):
    """
    read from a file a list of words needed by the feature extractor
    :param file_name: path of the file containing the words
    :return:feature list
    """
    with open(file_name) as f:
        feature_vect = f.read().splitlines()

    return feature_vect


def gender_feature(text, feature_vector):
    """
    Extract the gender features
    :param text:
    :param feature_vector: contains a bag of words
    :return: a dictionary which contains the feature and its computed value
    """

    # sentence length and vocab features
    tokens = word_tokenize(text.lower())
    sentences = sent_tokenize(text.lower())
    words_per_sent = np.asarray([len(word_tokenize(s)) for s in sentences])

    #bag_of_word features
    bag_feature = {}
    for word in feature_vector:
        bag_feature['contains(%s)' % word] = (word in set(tokens))

    #POS tagging features
    POS_tag = ['ADJ', 'ADV', 'DET', 'NOUN', 'PRT', 'VERB', '.']
    tagged_word = parse(text, chunks=False, tagset='UNIVERSAL').split()
    simplified_tagged_word = [(tag[0], map_tag('en-ptb', 'universal', tag[1])) for s in tagged_word for tag in s]
    freq_POS = nltk.FreqDist(tag[1] for tag in simplified_tagged_word if tag[1] in POS_tag)

    d = dict({'sentence_length_variation': words_per_sent.std()}, **bag_feature)

    return dict(d, **freq_POS)


def age_feature(text, feature_vector):
    """
    Extract age features
    :param text:
    :param feature_vector: contains a bag of words
    :return:a dictionary which contains the feature and its computed value
    """
    tokens = word_tokenize(text.lower())
    features = {}
    for word in feature_vector:
        features['contains(%s)' % word] = (word in set(tokens))

    d = dict(features, **dict({'FRE': textstat.flesch_reading_ease(text), 'FKGL': textstat.flesch_kincaid_grade(text)}))
    return d


def feature_apply(feature_extractor, feature_vector, attribute, number_of_file, corpus_path):
    """
    Extract features from each document
    :param feature_extractor: function that extract features
    :param feature_vector: contains a list of features
    :param attribute: indicate if the process for gender or age feature extraction
    :param number_of_file: number of document to be processed
    :return:vector that contain the extracted features
    """

    newcorpus = XMLCorpusReader(corpus_path, '.*')
    i = 0
    feature_set = []
    doc_list = newcorpus.fileids()

    for doc in doc_list[:number_of_file]:
        # print status
        i += 1
        if i % 50 == 0:
            print i
        doc = newcorpus.xml(doc)
        number_of_conversation = int(doc[0].attrib["count"])
        txt = " ".join([doc[0][j].text for j in range(number_of_conversation) if doc[0][j].text is not None])

        if textstat.sentence_count(txt) != 0:
            feature_set.append((feature_extractor(txt, feature_vector), doc.attrib[attribute]))

    return feature_set


def generate_classifier(feature_set, n_sample):
    """
    Train a NaiveBayes classifier with the training set.
    :param feature_set:
    :param n_sample: number of document
    :return:the trained classifier
    """

    classifier = nltk.NaiveBayesClassifier.train(feature_set)

    return classifier


def save_classifier(classifier, n_feat, n_sample):
    """
    Save the classifier in a pickle file with a noun indicating  number of documents
    and number of features used in to train the classifier
    :param classifier:
    :param n_feat: number of features
    :param n_sample: number of documents
    :return:
    """
    file_name = 'feat' + str(n_feat) + '_sample' + str(n_sample) + '.pickle'
    f = open(file_name, 'wb')
    pickle.dump(classifier, f)
    f.close()


def fetch_text(doc):
    """
    Process the text contained in a document
    :param doc:
    :return:processed text
    """
    txt = " ".join([doc[0][j].text for j in range(int(doc[0].attrib["count"])) if doc[0][j].text is not None])
    try:
        txt = BeautifulSoup(txt).get_text()
    except HTMLParser.HTMLParseError:
        txt = ""
    return txt


def extract_true_pred(file):
    """
    Extract the true age and gender from truth-en.txt file provided with test corpus
    :param file: path of the truth-en.txt file
    :return:dictionary which has name of xml file as a key and a list value composed from real age and gender values
    """
    f = open(file)
    true_pred = {}
    for line in f.readlines():
        line = line.split(":::")
        true_pred[line[0] + "_en_XXX_XXX.xml"] = [line[1], line[2][:3]]
    f.close()
    return true_pred


def test_set(corpus_dir, feature_extrator, vect_path, i):
    """
    Read ,process the test set and extract features from each document
    :param corpus_dir:path of the test set
    :param feature_extrator: function that extract features
    :param vect_path:
    :param i:index of class in the true_pred dictionay values; if 0 it refers to the gender 1 refers to the age
    :return:vector that contain the extracted features
    """
    vect = create_feature_vect(vect_path)
    newcorpus = XMLCorpusReader(corpus_dir, '.*')
    doc_list = newcorpus.fileids()
    test_feature_set = []
    true_pred = extract_true_pred(corpus_dir[:-2] + "truth-en.txt")

    j = 0
    for doc in doc_list:
        j += 1
        # print status
        if j % 10 == 0:
            print j
        xml_name = doc
        doc = newcorpus.xml(doc)
        txt = fetch_text(doc)
        if (textstat.sentence_count(txt) != 0) and (txt != ""):
            test_feature_set.append((feature_extrator(txt, vect), true_pred[xml_name][i]))

    return test_feature_set

def load_classifier(classifier_path):
    """
   load classifier
    :param location:location of the classifier
    """
    f = open(classifier_path)
    classifier = pickle.load(f)
    f.close()
    return classifier