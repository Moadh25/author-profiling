import nltk
from UtilFunc import test_set
from UtilFunc import load_classifier
from UtilFunc import gender_feature
from UtilFunc import age_feature

#Input
#path of the classifier to test
age_classifier_path="age_classifier.pickle"
gender_classifier_path='gender_classifier.pickle'
#path of the folder containing the test corpus
test_corpus_path = '/root/Downloads/TextMining/pan13-author-profiling-test-corpus2-2013-04-29/en'
#path of the file  containing specific words determing age
age_words_file='age_words.txt'
#path of the file  containing specific words determing gender
gender_words_file='gender_words.txt'


#index of class in the true_pred dictionay(0 for gender, 1 for age)
#index=1
#age_feature_set_test=test_set(test_corpus_path,age_feature,age_words_file,index)
#print feature_set_test

#load the age classifier
#age_classifier = load_classifier(age_classifier_path)

#print ' age accuracy', nltk.classify.accuracy(age_classifier, age_feature_set_test)

#index of class in the true_pred dictionay(0 for gender, 1 for age)
index=0
gender_feature_set_test=test_set(test_corpus_path,gender_feature,gender_words_file,index)
#print feature_set_test

#load the gender classifier
gender_classifier = load_classifier(gender_classifier_path)

print ' gender accuracy', nltk.classify.accuracy(gender_classifier, gender_feature_set_test)

