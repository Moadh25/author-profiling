import UtilFunc
from UtilFunc import create_feature_vect
from UtilFunc import generate_classifier
from UtilFunc import feature_apply
from UtilFunc import age_feature
from UtilFunc import gender_feature
from UtilFunc import save_classifier


#Input
#path of the file  containing specific words determing age
age_words_file='age_words.txt'
#path of the file  containing specific words determing gender
gender_words_file='gender_words.txt'
#path of the folder containg the corpus
corpus_root = '/root/Downloads/TextMining/pan13-author-profiling-training-corpus-2013-01-09/en'
#attribute
age_attribute='age_group'
gender_attribute='gender'
#number_of_file
number_of_file=236000

#extract words determing age
age_words=create_feature_vect(age_words_file)
#extract words determing gender
gender_words=create_feature_vect(gender_words_file)


age_feature_set=feature_apply(age_feature,age_words,age_attribute, number_of_file,corpus_root)

#generate the age classifier
age_classifier=generate_classifier(age_feature_set,1)

#save the age classifier
save_classifier(age_classifier,2,number_of_file)

gender_feature_set=feature_apply(gender_feature,gender_words,gender_attribute, number_of_file,corpus_root)

#generate the gender classifier
gender_classifier=generate_classifier(gender_feature_set,1)

#save the gender classifier
save_classifier(gender_classifier,3,number_of_file)