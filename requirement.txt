To run this project, it's requemended to use Pycharm, and install the following packages:
- nltk
-Pattern
-BeautifulSoup4
-textstat
-google-api-python-client

To execute the ClassifierGeneration.py, you need to have the 'en' folder from PAN dataset in the project folder 
To execute the ClassifierEvaluation.py, you need to have the 'pan13-test-corpus1\en' or 
the folder 'pan13-test-corpus2\en' from PAN dataset in the project folder
